// Sass configuration
const gulp = require('gulp');
const sass = require('gulp-sass');
const input = './scss/*.scss';
const output = './css';

const sassOptions = {
    errLogToConsole: true,
    outputStyle: 'compressed'
};

gulp.task('sass', function() {
    gulp.src(input)
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(gulp.dest(output))
});

gulp.task('default', ['sass'], function() {
    gulp.watch('scss/*.scss', ['sass']);
});
